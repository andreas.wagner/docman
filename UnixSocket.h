#include <optional>
#include <iostream>
  
#include <string.h>
#include <strings.h>
#include <cstring>
#include <errno.h>
#include <strings.h>
#include <sstream>

#include <sys/socket.h>
#include <sys/un.h>

#include "pollable.h"
#include "epoll_reactor.h"

class UnixSocket;

class UnixListenSocket : public Pollable
{
public:
	UnixListenSocket(std::filesystem::path path) :
	Pollable(socket(AF_UNIX, SOCK_STREAM, 0),
	EPOLLIN | EPOLLET),
	accepted_conn()
	{
		if(get_fd() < 1)
		{
			throw std::runtime_error("Fifo: socket() failed.");
		}
		sock.sun_family = AF_UNIX;
		std::memcpy(sock.sun_path, path.string().c_str(), path.string().size());
		*(sock.sun_path + path.string().size()) = '\0';
		/*if(std::filesystem::exists(path))
		{
			std::stringstream s;
			s << "UnixSocket Constructor, "
				<< path
				<< "exists!"
				<< std::endl;
			throw std::runtime_error(s.str());
		}*/
		int b = bind(get_fd(), (struct sockaddr*) &sock, sizeof(sock));
		if(b == -1)
		{
			std::stringstream s;
			s << "UnixSocket Constructor, bind(\"" << sock.sun_path << "\", "
				<< sizeof(sock) << ") failed: "
				<< errno << " "
				<< strerror(errno) << std::endl;
			throw std::runtime_error(s.str());
		}
		int l = listen(get_fd(), 5);
		if(l == -1)
		{
			std::stringstream s;
			s << "UnixSocket Constructor, listen() failed: "
				<< errno << " "
				<< strerror(errno) << std::endl;
			throw std::runtime_error(s.str());
		}
	}
	
	~UnixListenSocket()
	{
		close(get_fd());
	}
	
	std::optional<std::shared_ptr<UnixSocket>> get_connection()
	{
		std::optional<std::shared_ptr<UnixSocket>> ret_val = accepted_conn;
		accepted_conn.reset();
		return ret_val;
	}
	
	void readable() override
	{
		sockaddr addr;
		socklen_t len;
		accepted_conn = std::make_shared<UnixSocket>(accept(get_fd(), &addr, &len));
	}

	void writable() override
	{
		throw std::logic_error("writable() not implemented!");
	}

	void error() override
	{
		throw std::logic_error("error() not implemented!");
	}

	void pri() override
	{
		throw std::logic_error("pri() not implemented!");
	}

	void rdhup()
	{
		throw std::logic_error("rdhup() not implemented!");
	}

	void hup() override
	{
		throw std::logic_error("hup() not implemented!");
	}
private:
	std::optional<std::shared_ptr<UnixSocket>> accepted_conn;
	struct sockaddr_un sock;
};

class UnixSocket : public Pollable
{
public:
	UnixSocket(std::filesystem::path path):
	Pollable(
	socket(AF_UNIX, SOCK_STREAM, 0),
	EPOLLIN | EPOLLOUT |EPOLLET
	),
	rd_buf_ptr(NULL),
	wr_buf_ptr(NULL),
	read_len(-1),
	write_len(-1),
	expected_read(sizeof(read_len)),
	expected_write(0),
	read_progress(0),
	write_progress(0),
	message(std::make_shared<std::optional<std::string>>())
	{
		if(get_fd() < 1)
		{
			std::stringstream s;
			s << "Fifo: open() failed: " << errno << " - " << strerror(errno);
			throw std::runtime_error(s.str());
		}
		sock.sun_family = AF_UNIX;
		std::memcpy(sock.sun_path, path.string().c_str(), path.string().size());
		*(sock.sun_path + path.string().size()) = '\0';
		/*if(std::filesystem::exists(path))
		{
			std::stringstream s;
			s << "UnixSocket Constructor, "
				<< path
				<< "exists!"
				<< std::endl;
			throw std::runtime_error(s.str());
		}*/
		
		int c = connect(get_fd(), (struct sockaddr*) &sock, sizeof(sock));
		if(c == -1)
		{
			std::stringstream s;
			s << "UnixSocket Constructor, connect(\"" << sock.sun_path << "\", "
				<< sizeof(sock) << ") failed: "
				<< errno << " "
				<< strerror(errno) << std::endl;
			throw std::runtime_error(s.str());
		}
	}


	UnixSocket(int fd):
	Pollable(
	fd,
	EPOLLIN | EPOLLOUT | EPOLLET
	),
	rd_buf_ptr(NULL),
	wr_buf_ptr(NULL),
	read_len(0),
	write_len(0),
	expected_read(sizeof(read_len)),
	expected_write(0),
	read_progress(0),
	write_progress(0),
	message(std::make_shared<std::optional<std::string>>())
	{
		
	}

	~UnixSocket()
	{
		close(get_fd());
	}

	std::shared_ptr<std::optional<std::string>> get_message()
	{
		std::shared_ptr<std::optional<std::string>> return_value;
		if(message->has_value())
		{
			return_value = message;
			message=std::make_shared<std::optional<std::string>>();
			expected_read = sizeof(read_len);
			// free(rd_buf_ptr); // done in readable()
			return return_value;
		}
		return message;
	}

	void return_answer(std::string s)
	{
		expected_write = sizeof(write_len);
		sending = std::make_shared<std::optional<std::string>>(s);
		write_len = (*sending)->size();
		wr_buf_ptr = (*sending)->data();
		writable();
	}

	virtual void readable() override
	{
		if(expected_read > 0)
		{
			int diff = read(get_fd(), &read_len + sizeof(read_len) - expected_read, expected_read);
			if(diff > 0)
			{
				expected_read -= diff;
			}
			if(diff == -1 && errno != EAGAIN)
			{
				std::stringstream s;
				s << get_fd() << ", " << errno << " " << strerror(errno);
				throw std::runtime_error(s.str());
			}
		}
		if(expected_read == 0)
		{
			if(read_len < 1)
			{
				std::stringstream s;
				s << "read_len < 1: " << read_len;
				throw std::runtime_error(s.str());
			}
			if(rd_buf_ptr == NULL)
			{
				rd_buf_ptr = (char*) malloc(read_len+1);
				*(rd_buf_ptr+read_len+1) = '\0';
			}
			int diff = read(get_fd(), rd_buf_ptr + read_progress, read_len);
			if(diff > 0)
			{
				read_progress += diff;
				read_len -= diff;
			}
			if(diff == -1 && errno != EAGAIN)
				throw std::runtime_error(strerror(errno));
		}
		if(read_len == 0 && expected_read == 0)
		{
			message = std::make_shared<std::optional<std::string>>(std::string(rd_buf_ptr, 0, read_progress));
			expected_read = sizeof(read_len);
			free(rd_buf_ptr);
			rd_buf_ptr = NULL;
			read_progress = 0;
		}
	}

	virtual void writable() override
	{
		if(expected_write > 0 && write_len > 0)
		{
			int diff = write(get_fd(), &write_len + sizeof(write_len) - expected_write, expected_write);
			if(diff > 0)
			{
				expected_write -= diff;
			}
			if(diff == -1 && errno != EAGAIN)
				throw std::runtime_error(strerror(errno));
		}
		if(expected_write == 0 && write_len > 0)
		{
			int diff = write(get_fd(), wr_buf_ptr + write_progress, write_len);
			if(diff > 0)
			{
				write_progress += diff;
				write_len -= diff;
			}
			if(diff == -1 && errno != EAGAIN)
				throw std::runtime_error(strerror(errno));
		}
		if(write_len == 0)
		{
			expected_write = sizeof(write_len);
			write_progress = 0;
			write_len = 0;
		}
	}
	
	virtual void error() override
	{
		reactor->deregister(get_fd());
	}
	
	virtual void rdhup() override
	{
		reactor->deregister(get_fd());
	}

	virtual void hup() override
	{
		reactor->deregister(get_fd());
	}


private:
	std::shared_ptr<std::optional<std::string>> message;
	std::shared_ptr<std::optional<std::string>> sending;
	char *rd_buf_ptr;
	char *wr_buf_ptr;
	int expected_read;
	int expected_write;
	int read_len;
	int write_len;
	int read_progress;
	int write_progress;
	struct sockaddr_un sock;
};
