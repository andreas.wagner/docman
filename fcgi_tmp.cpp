#include <iostream>
#include <fcgio.h>
#include <fstream>
#include <cstdlib>
  
  
int main (void)
{
  std::streambuf *cin_streambuf = std::cin.rdbuf();
  std::streambuf *cout_streambuf = std::cout.rdbuf();
  std::streambuf *cerr_streambuf = std::cerr.rdbuf();
  
  FCGX_Request request;
  
  FCGX_Init();
  FCGX_InitRequest(&request, 0, 0);
  
  while(FCGX_Accept_r(&request) == 0)
  {
    fcgi_streambuf cin_fcgi_streambuf(request.in);
    fcgi_streambuf cout_fcgi_streambuf(request.out);
    fcgi_streambuf cerr_fcgi_streambuf(request.err);
    
    std::cin.rdbuf(&cin_fcgi_streambuf);
    std::cout.rdbuf(&cout_fcgi_streambuf);
    std::cerr.rdbuf(&cerr_fcgi_streambuf);
    
    char *length_string = FCGX_GetParam("CONTENT_LENGTH", request.envp);
    char *end;
    long long length = std::strtoll(length_string, &end, 10);
    while(length > 0)
    {
    }
    std::cout << "Content-Type: text/plain\r\n";
    std::cout << "Content-Encofing: UFT-8\r\n";
    std::cout << "Content-Length: " << response_part.length() << "\r\n\r\n";
    std::cout << response_part;
  }
  
  std::cin.rdbuf(cin_streambuf);
  std::cout.rdbuf(cout_streambuf);
  std::cerr.rdbuf(cerr_streambuf);
}