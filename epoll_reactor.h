#ifndef _epoll_reactor_h
#define _epoll_reactor_h

#include <map>
#include <set>
#include "pollable.h"
#include <memory>

class EpollReactor
{
public:
	EpollReactor();
	~EpollReactor();
	
	void registerPollable(std::shared_ptr<Pollable> p);
	void deregister(int fd);
	void deregister_all_except(std::set<int> fds);
	void wait_and_exec();
	std::shared_ptr<std::set<std::shared_ptr<Pollable>>> getActivity();
	
private:
	std::map<int, std::shared_ptr<Pollable>> pollable_map;
	std::shared_ptr<std::set<std::shared_ptr<Pollable>>> activity;
	int epoll_fd;
	int events_per_iteration;
};

#endif
