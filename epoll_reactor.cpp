#include "epoll_reactor.h"
#include <sys/epoll.h>
#include <errno.h>
#include <string.h>
#include <sstream>
#include <unistd.h>

EpollReactor::EpollReactor():
epoll_fd(epoll_create(16)),
events_per_iteration(16),
activity(std::make_shared<std::set<std::shared_ptr<Pollable>>>()),
pollable_map()
{
	if(epoll_fd == -1)
	{
		std::stringstream error_msg_builder("Could not create epoll_fd: ");
		error_msg_builder << errno << ", " << strerror(errno);
		throw std::runtime_error(error_msg_builder.str());
	}
}

EpollReactor::~EpollReactor()
{
	if(epoll_fd != -1)
		close(epoll_fd);
}

void EpollReactor::registerPollable(std::shared_ptr<Pollable> p)
{
	if(epoll_fd == -1)
		throw std::logic_error("registerPollable called for wrongly initialized EpollReactor!");
		
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, p->get_fd(), p->get_event_ptr());
	pollable_map[p->get_fd()] = p;
	p->registerReactor(this);
}

void EpollReactor::deregister(int fd)
{
	epoll_ctl(epoll_fd, EPOLL_CTL_DEL, fd, NULL);
	auto to_del = pollable_map[fd];
	if(activity->count(to_del) > 0)
		activity->erase(to_del);
	pollable_map.erase(fd);
}

void EpollReactor::deregister_all_except(std::set<int> fds)
{
	for(auto & pair : pollable_map)
	{
		if(fds.count(pair.first) == 0)
			deregister(pair.first);
	}
	close(epoll_fd);
}

void EpollReactor::wait_and_exec()
{
	struct epoll_event events[events_per_iteration];
	int n_events = epoll_wait(epoll_fd, events, events_per_iteration, 500);
	activity->clear();
	for(int i = 0; i < n_events; i++)
	{
		if(pollable_map.count(events[i].data.fd) > 0)
			if(activity->count(pollable_map[events[i].data.fd]) == 0)
				activity->insert(pollable_map[events[i].data.fd]);
		if((events[i].events & EPOLLIN) == EPOLLIN)
		{
			if(pollable_map.count(events[i].data.fd) > 0)
				pollable_map[events[i].data.fd]->readable();
		}
		if((events[i].events & EPOLLOUT) == EPOLLOUT)
		{
			if(pollable_map.count(events[i].data.fd) > 0)
				pollable_map[events[i].data.fd]->writable();
		}
		if((events[i].events & EPOLLPRI) == EPOLLPRI)
		{
			if(pollable_map.count(events[i].data.fd) > 0)
				pollable_map[events[i].data.fd]->pri();
		}
		if((events[i].events & EPOLLRDHUP) == EPOLLRDHUP)
		{
			if(pollable_map.count(events[i].data.fd) > 0)
				pollable_map[events[i].data.fd]->rdhup();
		}
		if((events[i].events & EPOLLERR) == EPOLLERR)
		{
			if(pollable_map.count(events[i].data.fd) > 0)
				pollable_map[events[i].data.fd]->error();
		}
		if((events[i].events & EPOLLHUP) == EPOLLHUP)
		{
			if(pollable_map.count(events[i].data.fd) > 0)
				pollable_map[events[i].data.fd]->hup();
		}
		
	}
}

std::shared_ptr<std::set<std::shared_ptr<Pollable>>> EpollReactor::getActivity()
{
	return activity;
}
