#include "pollable.h"
#include <stdexcept>

Pollable::Pollable(int fd_to_poll, int op):
fd(fd_to_poll),
op(op)
{
	event.events = op;
	event.data.fd = fd;
};

Pollable::~Pollable()
{
}

int Pollable::get_fd()
{
	return fd;
}

struct epoll_event * Pollable::get_event_ptr()
{
	return &event;
}

void Pollable::registerReactor(EpollReactor * react)
{
	reactor = react;
}

EpollReactor * Pollable::getReactorPtr()
{
	return reactor;
}

void Pollable::readable()
{
	throw std::logic_error("readable() not implemented!");
}

void Pollable::writable()
{
	throw std::logic_error("writable() not implemented!");
}

void Pollable::error()
{
	throw std::logic_error("error() not implemented!");
}

void Pollable::pri()
{
	throw std::logic_error("pri() not implemented!");
}

void Pollable::rdhup()
{
	throw std::logic_error("rdhup() not implemented!");
}

void Pollable::hup()
{
	throw std::logic_error("hup() not implemented!");
}

