#include "process-man.h"

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>

void set_nonblocking(int fd)
{
	int save_fd = fcntl(fd, F_GETFL);
	save_fd |= O_NONBLOCK;
	fcntl(fd, F_SETFL, save_fd);
}

Buffer::Buffer(int size):
	_buf(new char[size]),
	_size(size),
	_consumed(0)
{
}

Buffer::Buffer():
	_buf(NULL),
	_size(0),
	_consumed(0)
{
}

Buffer::~Buffer()
{
	delete _buf;
}

void Buffer::reset()
{
	delete _buf;
	_buf = NULL;
	_size = 0;
	_consumed = 0;
}

char *Buffer::ptr()
{
	return _buf + _consumed;
}

int Buffer::size()
{
	return _size - _consumed;
}

void Buffer::consume(int count)
{
	this->_consumed += count;
	if(this->_consumed > _size)
		throw std::logic_error("Buffer consumed more than size.");
}


ProcessFD::ProcessFD(int fd_to_poll, int op, Process * p):
	Pollable(fd_to_poll, op),
	ptr(p)
{
}

ProcessFD::~ProcessFD()
{
}

void ProcessFD::readable()
{
	ptr->perform_io_out();
}

void ProcessFD::writable()
{
	ptr->perform_io_err();
}

void ProcessFD::error()
{
	ptr->perform_io_in();
}

void ProcessFD::hup()
{
	reactor->deregister(get_fd());
	if(ptr->is_running())
		ptr->wait();
}


int err_handler(char * ptr, int size)
{
	if(size == 0)
		return 0;
	std::string data(ptr, size);
	std::cerr << data;
	return 0;
};

Process::Process(
		std::filesystem::path wd,	///< Working Directory
		std::string command,		///< Command Name
		std::list<std::string> args	///< Argument-list
		):
	_wd(wd),
	_command(command),
	_args(args),
	_on_exit_fn([](unsigned char){return 0;}),
	_on_write_fn([](char*, int){return 0;}),
	_on_error_str_fn(err_handler),
	_running(false)
{
}

Process::~Process()
{
}

long long Process::getMemoryConsumption()
{
	std::filesystem::path status_file("/proc/");
	status_file /= std::to_string(pid);
	status_file /= "status";
	std::ifstream in_file(status_file);
	while(in_file.good())
	{
		std::string line;
		std::getline(in_file, line);
		int pos;
		if((pos = line.find("VmRSS:")) != std::string::npos)
		{
			std::string sub = line.substr(pos+6, std::string::npos);
			char * end;
			return std::strtoll(sub.c_str(), &end, 10);
		}
	}
	return -1;
}

void Process::onExit(std::function<int(unsigned char)> fn)
{
	_on_exit_fn = fn;
}

std::function<int(unsigned char)> Process::getExitFn()
{
	return _on_exit_fn;
}

void Process::onOutStr(std::function<int(char*, int)> fn)
{
	_on_write_fn = fn;
}

void Process::onErrorStr(std::function<int(char*, int)> fn)
{
	_on_error_str_fn = fn;
}

void Process::putToStdin(std::shared_ptr<Buffer> buf)
{
	_child_stdin_buf.push_back(buf);
}

void Process::start()
{
	int ret1 = pipe(fd1);
	if(ret1 != 0)
		throw std::runtime_error("pipe1");
	int ret2 = pipe(fd2);
	if(ret2 != 0)
		throw std::runtime_error("pipe2");
	int ret3 = pipe(fd3);
	if(ret3 != 0)
		throw std::runtime_error("pipe3");
	
	pid = fork();
	if(pid == -1)
	{
		throw std::runtime_error("fork() failed!");
	}
	else if(pid == 0) // this is the child-process
	{
		
		if(dup2(fd1[0], STDIN_FILENO) == -1)
			throw std::runtime_error("dup2");
		if(dup2(fd2[1], STDOUT_FILENO) == -1)
			throw std::runtime_error("dup2");
		if(dup2(fd3[1], STDERR_FILENO) == 1)
			throw std::runtime_error("dup2");
		close(fd1[0]);
		close(fd2[0]);
		close(fd3[0]);
		close(fd1[1]);
		close(fd2[1]);
		close(fd3[1]);
		
		if(chdir(_wd.c_str()) != 0)
		{
			std::stringstream err_str;
			err_str << "chdir(" << _wd.c_str() << "): "
			<< errno << " " << strerror(errno);
			throw std::runtime_error(err_str.str());
		}
		
		int end = _args.size()+1;
		cmdline = new char*[end];
		cmdline[0] = (char *) _command.c_str();
		auto iter = _args.cbegin();
		
		for(int i = 1; i < end; i++)
		{
			cmdline[i] = (char *) iter->c_str();
			iter++;
		}
		
		cmdline[end] = NULL;
		
		in->getReactorPtr()->deregister_all_except(
			std::set<int>{STDIN_FILENO, STDOUT_FILENO, STDERR_FILENO});
		
		int ret = execvp(_command.c_str(), cmdline);
		std::cout << ret << std::endl;
	}
	else
	{
		set_nonblocking(fd1[1]);
		set_nonblocking(fd2[0]);
		set_nonblocking(fd3[0]);
		close(fd1[0]);
		close(fd2[1]);
		close(fd3[1]);
		in = std::make_shared<ProcessFD>(fd1[1], EPOLLOUT | EPOLLET, this);
		out = std::make_shared<ProcessFD>(fd2[0], EPOLLIN | EPOLLET, this);
		err = std::make_shared<ProcessFD>(fd3[0], EPOLLIN | EPOLLET, this);
		_running = true;
	}
}

bool Process::is_running()
{
	return _running;
}

std::list<std::shared_ptr<Pollable>> Process::getPollables()
{
	return {in, out, err};
}

int Process::getPID()
{
	return pid;
}

void Process::closeFD(fd_type t)
{
	switch(t)
	{
	case Estdin:
		close(fd1[1]);
		break;
	case Estdout:
		close(fd2[0]);
		break;
	case Estderr:
		close(fd3[0]);
		break;
	}
}

void Process::wait()
{
	int status;
	waitpid(pid, &status, 0);
	_running = false;
	
	_on_exit_fn(WEXITSTATUS(status));
}

int Process::perform_io_out()
{
	if(!_running)
		return 0;
	
	int n = 65000;
	char buf[n];
	int processedA = read(fd2[0], buf, n);
	if(processedA != -1)
	{
		_on_write_fn(buf, processedA);
		if(processedA == 0)
		{
			//wait();
		}
	}
	else
	{
		if(errno != EAGAIN)
		{
			std::cout << strerror(errno);
			std::cout.flush();
		}
	}
	return 0;
}

int Process::perform_io_in()
{
	if(!_running || (_child_stdin_buf.begin() == _child_stdin_buf.end()))
		return 0;
	auto input = *_child_stdin_buf.begin();
	if(input->ptr() != NULL)
	{
		int written = write(fd1[1], input->ptr(), input->size());
		if(written < input->size())
		{
			input->consume(written);
		}
		else if (written == input->size())
		{
			_child_stdin_buf.pop_front();
		}
		else
		{
			throw std::runtime_error("Writing went wrong.");
		}
	}
	return 0;
}

int Process::perform_io_err()
{
	if(!_running)
		return 0;
	
	int n = 65000;
	char buf[n];
	int processedA = read(fd3[0], buf, n);
	if(processedA != -1)
	{
		_on_error_str_fn(buf, processedA);
		if(processedA == 0)
		{
			//wait();
		}
	}
	else
	{
		if(errno != EAGAIN)
		{
			std::cout << strerror(errno);
			std::cout.flush();
		}
	}
	return 0;
}


QueueElement::QueueElement(
	std::shared_ptr<Process> pr,
	long long expectedMemConsumption):
	_pr(pr),
	_expectedMemConsumption(expectedMemConsumption)
{
}

QueueElement::~QueueElement()
{
}

std::shared_ptr<Process> QueueElement::getProcess()
{
	return _pr;
}

long long QueueElement::getExpectedMemConsumption()
{
	return _expectedMemConsumption;
}



std::shared_ptr<ProcessManager> ProcessManager::create(
	int maxProcesses,
	long long reserveMaxMemory,
	std::shared_ptr<EpollReactor>  rea)
{
	return std::shared_ptr<ProcessManager>(new ProcessManager(
		maxProcesses,
		reserveMaxMemory,
		rea));
}

ProcessManager::ProcessManager(
	int maxProcesses,
	long long reservedMaxMemory,
	std::shared_ptr<EpollReactor> react):
	reserved_processes(maxProcesses),
	reserved_processes_avail(maxProcesses),
	reserved_mem(reservedMaxMemory),
	reserved_mem_avail(reservedMaxMemory),
	reactor(react)
{
}

std::shared_ptr<ProcessManager> ProcessManager::getptr()
    {
        return shared_from_this();
    }
    
ProcessManager::~ProcessManager()
{
	// Consider waiting for all Processes
}

long long ProcessManager::reservedMemAvail()
{
	return reserved_mem_avail;
}

int ProcessManager::reservedProcessesAvail()
{
	return reserved_processes_avail;
}

void ProcessManager::addProcess(std::shared_ptr<Process> pr, long long expectedMemConsumptionkB)
{
	queue_waiting.push_back(std::make_shared<QueueElement>(pr, expectedMemConsumptionkB));
	std::function<int(unsigned char)> fn = pr->getExitFn();
	
	std::shared_ptr<ProcessManager> self = getptr();
	pr->onExit(
		[pr, self, fn](unsigned char code)
		{
			self->queueElementFinished(pr->getPID());
			return fn(code);
		});
		
}

void ProcessManager::queueElementFinished(int pid)
{
	reserved_mem_avail += queue_running[pid];
	reserved_processes_avail++;
	queue_running.erase(pid);
}

void register_pollables(std::shared_ptr<EpollReactor> & r, std::list<std::shared_ptr<Pollable>> p)
{
	for(auto & i : p)
		r->registerPollable(i);
}

void ProcessManager::iterate()
{
	while((queue_waiting.size() > 0)
		&& (reserved_processes_avail > 0))
	{
		std::shared_ptr<QueueElement> qe = queue_waiting.front();
		
		if(reserved_mem_avail > qe->getExpectedMemConsumption())
		{
			qe->getProcess()->start();
			queue_waiting.pop_front();
			reserved_mem_avail -= qe->getExpectedMemConsumption();
			reserved_processes_avail--;
			
			register_pollables(reactor, qe->getProcess()->getPollables());
			
			queue_running[qe->getProcess()->getPID()]
				= qe->getExpectedMemConsumption();
		}
		else
		{
			break;
		}
	}
	reactor->wait_and_exec();
}
