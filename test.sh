#!/bin/bash

sudo service apache2 stop
killall scan_dir_observer
make
if [ $? != 0 ]
then
	exit 1
fi

sudo ./reset_sem
sudo rm /var/www/html/fcgi/fcgi1.fcgi
sudo cp fcgi1 /var/www/html/fcgi/fcgi1.fcgi
sudo chown www-data:www-data /var/www/html/fcgi/fcgi1.fcgi
sudo chmod ug+s /var/www/html/fcgi/fcgi1.fcgi
#sudo rm -rf /var/www/html/fcgi/1*
#sudo rm -rf /var/www/html/fcgi/docman
#sudo rm -rf /mnt/cmd_fifos/*
sudo chown www-data:www-data scan_dir_observer
sudo chmod ug+s scan_dir_observer
sudo service apache2 start
#gdb ./scan_dir_observer
./scan_dir_observer

