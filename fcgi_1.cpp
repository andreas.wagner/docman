#include <iostream>
#include <fcgio.h>
#include <fstream>
#include <cstdlib>
#include <filesystem>
  
#include <chrono>
#include <thread>
  
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <semaphore.h>
#include <string.h>

#include "UnixSocket.h"
#include "epoll_reactor.h"



int main (void)
{
	umask(0007);
	std::string error(".");
	std::fstream error_log("/var/www/html/fcgi/test.log", std::ios_base::out);

	std::shared_ptr<std::optional<std::string>> answ = std::make_shared<std::optional<std::string>>();
	std::shared_ptr<UnixSocket> to_obs_socket;
	EpollReactor reactor;

	

	std::streambuf *cin_streambuf = std::cin.rdbuf();
	std::streambuf *cout_streambuf = std::cout.rdbuf();
	std::streambuf *cerr_streambuf = std::cerr.rdbuf();

	FCGX_Request request;

	FCGX_Init();
	FCGX_InitRequest(&request, 0, 0);

	while(FCGX_Accept_r(&request) == 0)
	{
		std::filesystem::path acceptor_path("/var/www/html/fcgi/docman");
		try
		{
			if(!to_obs_socket)
				to_obs_socket = std::make_shared<UnixSocket>(acceptor_path);
		}
		catch(std::runtime_error e)
		{
			error_log << "while initializing UnixSocket " << e.what();
			error_log.flush();
			abort();
		}
		reactor.registerPollable(to_obs_socket);
		fcgi_streambuf cin_fcgi_streambuf(request.in);
		fcgi_streambuf cout_fcgi_streambuf(request.out);
		fcgi_streambuf cerr_fcgi_streambuf(request.err);

		std::cin.rdbuf(&cin_fcgi_streambuf);
		std::cout.rdbuf(&cout_fcgi_streambuf);
		std::cerr.rdbuf(&cerr_fcgi_streambuf);

		std::cout << "Content-Type: text/plain\r\n";
		std::cout << "Content-Encoding: UFT-8\r\n";
		std::cout.flush();

		char *length_string = FCGX_GetParam("CONTENT_LENGTH", request.envp);
		char *end;
		long long length = std::strtoll(length_string, &end, 10);
		std::stringstream concat_input;
		bool sent_something = false;
		std::stringstream to_obs;
		while(length > 0 && std::cin.good())
		{
			std::string input;
			std::getline(std::cin, input);
			//std::cerr << input << "\n";
			if(input.length() > 0)
			{
				to_obs << input;
				sent_something = true;
			}
			length -= input.length();
			concat_input << input << "<br />";
		}
		std::shared_ptr<std::optional<std::string>> s = std::make_shared<std::optional<std::string>>();
		try
		{
			to_obs_socket->return_answer(to_obs.str());
		}
		catch(std::runtime_error e)
		{
			error_log << " ex " <<e.what() << std::endl;
			error_log.flush();
			abort();
		}
		while(!(s->has_value()))
		{
			try
			{
				reactor.wait_and_exec();
				s = to_obs_socket->get_message();
			}
			catch(std::runtime_error e)
			{
				error_log << e.what();
				error_log.flush();
				abort();
			}
		}
		std::stringstream response_part;
		response_part << concat_input.str() << "<br />\n**s=" << **s << "<br />\nerror=" << error;

		std::cout << "200: OK\r\n";
		std::cout << "Content-Length: " << response_part.view().size() << "\r\n\r\n";
		std::cout << response_part.view();
	}

	std::cin.rdbuf(cin_streambuf);
	std::cout.rdbuf(cout_streambuf);
	std::cerr.rdbuf(cerr_streambuf);

	return 0;
}
