#ifndef _pollable_h
#define _pollable_h
#include <map>
#include <sys/epoll.h>

class EpollReactor;


class Pollable
{
public:
	Pollable(int fd_to_poll, int op);
	~Pollable();
	
	int get_fd();
	struct epoll_event * get_event_ptr();
	
	void registerReactor(EpollReactor * react);
	EpollReactor * getReactorPtr();
	
	virtual void readable();
	virtual void writable();
	virtual void error();
	virtual void pri();
	virtual void rdhup();
	virtual void hup();
	
protected:
	EpollReactor * reactor;

private:
	int fd;
	int op;
	struct epoll_event event;
};

#endif
