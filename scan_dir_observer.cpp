#include <iostream>
#include <fstream>
#include <filesystem>
#include <set>
#include <list>
#include <map>
#include <chrono>
#include <thread>
#include <memory>
#include <string>
#include <optional>

#include <sys/inotify.h>
#include <unistd.h>
#include <poll.h>
#include <wait.h>
#include <signal.h>
#include <stdlib.h>
  
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <pwd.h>
  
#include "UnixSocket.h"
#include "pollable.h"
#include "process-man.h"

class DirObserver : public Pollable
{
public:
  DirObserver(std::filesystem::path dir) :
  Pollable(inotify_init1(IN_NONBLOCK), EPOLLIN | EPOLLET),
  dir_to_track(dir),
  modified_files(std::make_shared<std::map<std::filesystem::path, std::chrono::time_point<std::chrono::system_clock>>>()),
  ready_files(std::make_shared<std::set<std::filesystem::path>>())
  {
    if(get_fd() == -1)
    {
      throw std::runtime_error("inotify_init1 failed");
    }
    if(inotify_add_watch(get_fd(), dir_to_track.string().c_str(), IN_CLOSE | IN_MODIFY | IN_DELETE) == -1)
    {
      throw std::runtime_error("inotify_add_watch failed for " + dir_to_track.string());
    }

    for(auto dir_entry : std::filesystem::directory_iterator{dir_to_track})
    {
      std::filesystem::path p = dir_entry.path();
      if(std::filesystem::is_regular_file(p) && ready_files->count(p) == 0)
      {
        ready_files->insert(p);
      }
    }
  }
  
  ~DirObserver()
  {
    close(get_fd());
  }

  void readable()
  {  
    char buf[4*1024];
    struct inotify_event *event;
    int len = 1;
    while(len != -1)
    {  
      len = read(get_fd(), buf, sizeof(buf));
      char *ptr;

      // evaluate inotify events
      for(ptr = buf;
          ptr < buf + len;
          ptr += sizeof(struct inotify_event) + event->len)
      {
        event = (struct inotify_event *) ptr;
        if(event->len > 0 && !(event->mask & IN_ISDIR))
        {
          std::filesystem::path p(dir_to_track/event->name);
          if(event->mask & IN_MODIFY)
          {
            (*modified_files)[p] = std::chrono::system_clock::now();
          }
          if(event->mask & IN_CLOSE_WRITE)
          {
            if(ready_files->count(p) == 0)
              ready_files->insert(p);
            if(modified_files->count(p) != 0)
            {
              modified_files->erase(p);
            }
          }
          else if(event->mask & IN_DELETE)
          {
            if(ready_files->count(p) != 0)
              ready_files->erase(p);
            if(modified_files->count(p) != 0)
              modified_files->erase(p);
          }
          else if(event->mask & IN_MOVED_FROM)
          {
            if(ready_files->count(p) != 0)
              ready_files->erase(p);
            if(modified_files->count(p) != 0)
              modified_files->erase(p);
          }
          else if(event->mask & IN_MOVED_TO)
          {
            if(ready_files->count(p) == 0)
              ready_files->insert(p);
          }
        }
      }
    }
  }
  
  std::shared_ptr<std::set<std::filesystem::path>> getReadyFiles()
  {
    std::shared_ptr<std::set<std::filesystem::path>> return_value
      = std::make_shared<std::set<std::filesystem::path>>();
    std::shared_ptr<std::set<std::filesystem::path>> remove_from_map
      = std::make_shared<std::set<std::filesystem::path>>();
    *return_value = *ready_files; 
    
    // remove modified files from ready-set
    for(auto const &p : *modified_files)
    {
      if(return_value->count(p.first) != 0)
      {
        if(std::chrono::system_clock::now() - p.second < std::chrono::seconds(4))
          return_value->erase(p.first);
        else
        {
          remove_from_map->insert(p.first);
        }
      }
    }
    for(auto const & i : *remove_from_map)
    {
      modified_files->erase(i);
    }
    return return_value;
  }
  
  void rdhup()
  {
	  reactor->deregister(get_fd());;
  }
  
  void pri()
  {
  }
  
  private:
  std::shared_ptr<std::map<std::filesystem::path, std::chrono::time_point<std::chrono::system_clock>>> modified_files;
  std::shared_ptr<std::set<std::filesystem::path>> ready_files;
  std::filesystem::path dir_to_track;
};

volatile bool go_on = true;

void handler(int num)
{
  std::cerr << "quitting" << std::endl;
  go_on = false;
}

void my_exit()
{
}

int main(int argc, char *argv[])
{
	signal(SIGHUP, handler);
	signal(SIGTERM, handler);
	signal(SIGINT, handler);
	std::shared_ptr<UnixListenSocket> acceptor;
	std::shared_ptr<DirObserver> obs1;
	
	std::cerr << getuid() << ": " << getpwuid(getuid())->pw_name << "\n"
		 << geteuid() << ": " << getpwuid(geteuid())->pw_name << std::endl;
		 
	std::filesystem::path acceptor_path("/var/www/html/fcgi/docman");
	try
	{
		if(std::filesystem::exists(acceptor_path))
		{
			std::filesystem::remove(acceptor_path);
		}
		acceptor = std::make_shared<UnixListenSocket>(acceptor_path);
		obs1 = std::make_shared<DirObserver>("/mnt/scan/");
	}
	catch(std::runtime_error e)
	{
		std::cerr << "init: runtime-error:" << e.what();
		abort();
	}
	catch(std::exception e)
	{
		std::cerr << "init: exception: " << e.what();
		abort();
	}
	std::shared_ptr<EpollReactor> reactor;
	reactor->registerPollable(acceptor);
	reactor->registerPollable(obs1);
	
	std::shared_ptr<ProcessManager> proc_man
		= ProcessManager::create(4, 1000000, reactor);
	
	while(go_on)
	{
		proc_man->iterate();
		std::optional<std::shared_ptr<UnixSocket>> conn = acceptor->get_connection();
		if(conn.has_value())
		{
			reactor->registerPollable(*conn);
		}
		for(auto & a : *reactor->getActivity())
		{
			if((a.get() != obs1.get()) && (a.get() != acceptor.get()))
			{
				auto msg = (dynamic_cast<UnixSocket *>(a.get()))->get_message();
				if(msg->has_value())
				{
					std::stringstream s;
					s << **msg << "<br />\n";
					auto set = obs1->getReadyFiles();
					for(auto rf : *set)
						s << rf << "<br />\n";
					(dynamic_cast<UnixSocket *>(a.get()))->return_answer(s.str());
				}
			}
		}
	}
	if(std::filesystem::exists(acceptor_path))
	{
		std::filesystem::remove(acceptor_path);
	}
	return 0;
}
