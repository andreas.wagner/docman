#include <semaphore.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <iostream>
  
int main(void)
{
  sem_unlink("/fcgi1_semaphore");
  sem_t *my_sem = sem_open("/fcgi1_semaphore", O_CREAT, 0664, 1);

  if(my_sem == SEM_FAILED)
  {
    std::cout << "error:" << strerror(errno) << std::endl;
    return -1;
  }
  int sval = 0;
  sem_getvalue(my_sem, &sval);
  while(sval < 1)
  {
    sem_post(my_sem);
    sem_getvalue(my_sem, &sval);
  }
  sem_close(my_sem);
  sem_unlink("/fcgi1_semaphore");
}