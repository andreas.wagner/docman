#ifndef _process_man_h_
#define _process_man_h_

#include <list>
#include <map>
#include <filesystem>
#include <functional>
#include <memory>

#include "pollable.h"
#include "epoll_reactor.h"

class Process; // forward declaration

/** Buffer
 */
class Buffer
{
public:
	/** Constructor for Buffers of size size,
	 * @param size Size to allocate
	 */
	Buffer(int size);
	
	/** Constructor for empty, invalif buffers.
	 * ptr() will return NULL.
	 */
	Buffer();
	
	/** Destructor
	 */
	~Buffer();
	
	/** Pointer to the managed buffer.
	 * 
	 * @return Pointer to the managed buffer of size size. (moved by
	 * consume())
	 */
	char *ptr();
	
	/** Size of the buffer
	 * 
	 * @return Size of the buffer.
	 */
	int size();
	
	/** Move the pointer returned by ptr().
	 * @param count Number of bytes to proceed in buffer.
	 */
	void consume(int count);
	
	/** Deallocates buffer and invalidates all data.
	 * I currently don'T see why I have that...
	 */
	void reset();
	
	
private:
	char * _buf;
	int _size;
	int _consumed;
};

/** Filedescriptor-Pollable used by the Process-class
 */
class ProcessFD : public Pollable
{
public:
	/** Constructor, assigning a Pollable to a Process.
	 * @param fd_to_poll handled by Pollable
	 * @param op handled by Pollable
	 * @param p Process to wait wor on hangup.
	 * 
	 * TODO: Consider disabling deregistering on hangup. I suppose,
	 * STDIN, STDOUT or STDERR may be reconnected after close().
	 */
	ProcessFD(int fd_to_poll, int op, Process * p);
	
	/** Destructor
	 */
	~ProcessFD();
	
	/** process output
	 */
	virtual void readable() override;
	
	/** process input
	 */
	virtual void writable() override;
	
	/** process error-conditions
	 */
	virtual void error() override;
	
	/** deregister and waitpid() for process
	 */
	virtual void hup() override;

private:
	Process * ptr;
};

/** Class to spawn a process and to handle input and (error-)output.
 */
class Process
{
public:
	/** Needed for closing filedescriptors when shutting down a process.
	 */
	enum fd_type
	{
		Estdin,		///< STDIN
		Estdout,	///< STDOUT
		Estderr		///< STDERR
	};

	/** Constructor
	 */
	Process(
		std::filesystem::path wd,	///< Working Directory
		std::string command,		///< Command Name
		std::list<std::string> args	///< Argument-list
		);
		
	/** Destructor
	 */
	~Process();
	
	/** get current memory-consumption from /proc/$pid/status, line VmRSS
	 */
	long long getMemoryConsumption();
	
	/** Set Function to be called on process' exit.
	 * @param fn Function to be called on process' exiting.
	 */
	void onExit(std::function<int(unsigned char)> fn);
	
	/** Add a Buffer to the process' STDIN-stream.
	 * @param buf Buffer to send.
	 */
	void putToStdin(std::shared_ptr<Buffer> buf);
	
	/** Handle output of process.
	 * @param fn function to handle output; prototype is
	 * 		int my_function(ptr, size);
	 */
	void onOutStr(std::function<int(char*, int)> fn);
	
	/** Handle error-output of stream.
	 * @param fn function to handle error-output; prototype is
	 * 		int my_function(ptr, size);
	 */
	void onErrorStr(std::function<int(char*, int)> fn);
	
	/** return the current exit-function, which would be called after
	 * the process' termination.
	 *
	 * @return function, which would be called after the process'
	 * 		exiting.
	 */
	std::function<int(unsigned char)> getExitFn();
	
	/** start the process
	 */
	void start();
	
	/** true if process is running
	 * @return true if process is running
	 */
	bool is_running();
	
	/** close filedescriptor of corresponding stream.
	 * @param fd fd_type to close.
	 * 
	 * Needed for correct shutdown of a process waiting for EOF.
	 */
	void closeFD(fd_type fd);
	
	/** Wait for the end of a process.
	 * 
	 * Calls waitpid().
	 */
	void wait();
	
	/** returns the PID of the process.
	 * 
	 * @return pid of the process.
	 */
	int getPID();
	
	//int perform_io();
	
	/** Perform input-actions.
	 */
	int perform_io_in();
	
	/** Perform output-actions.
	 */
	int perform_io_out();
	
	/** Perform error-actions.
	 */
	int perform_io_err();
	
	/** Returns the Pollables of STDIN, STDOUT and STDERR for use in
	 * EpollReactor.
	 */
	std::list<std::shared_ptr<Pollable>> getPollables();
	
private:

	std::function<int(char*, int)> _on_write_fn;
	std::function<int(char*, int)> _on_error_str_fn;
	std::function<int(unsigned char)> _on_exit_fn;
	volatile bool _running;
	
	std::filesystem::path _wd;
	std::string _command;
	std::list<std::string> _args;

	int fd1[2], fd2[2], fd3[2];
	std::shared_ptr<Pollable> in;
	std::shared_ptr<Pollable> out;
	std::shared_ptr<Pollable> err;
	
	int pid;
	char ** cmdline;
	
	std::list<std::shared_ptr<Buffer>> _child_stdin_buf;
};


/** Element of the ProcessManagers queue.
 */
class QueueElement
{
public:
	/** Constructor
	 * @param pr shared pointer to a process, which is about to be started.
	 * @param expectedMemoryConsumption Expected Amount of Memory, used
	 * 		by this process.
	 */
	QueueElement(std::shared_ptr<Process> pr, long long expectedMemConsumption);
	
	/** Destructor
	 */
	~QueueElement();
	
	/** Returns the shared pointer passed as parameter of the
	 * constructor.
	 * 
	 * @return Shared pointer to a Process which was passed as argument
	 * 		to the constructor.
	 */
	std::shared_ptr<Process> getProcess();
	
	/** Returns the expected memory-consumption, passed to the
	 * constructor.
	 * 
	 * @return Expected memoryconsumption.
	 * 
	 * */
	long long getExpectedMemConsumption();

private:
	std::shared_ptr<Process> _pr;
	long long _expectedMemConsumption;
};


/** Class to manage Processes
 */
class ProcessManager : public std::enable_shared_from_this<ProcessManager>
{
public:
	/** create a ProcessManager
	 * @
	 */
	[[nodiscard]] static std::shared_ptr<ProcessManager>
	create(
		int maxProcesses,			///< maximum number of processes to run in parallel
		long long reserveMaxMemory,	///< maximum amount of memory to be expected to be consumed
		std::shared_ptr<EpollReactor>  reactor
	);
	
	/** Destructor
	 */
	~ProcessManager();
	
	/** get shared pointer to this process-manager
	 */
	std::shared_ptr<ProcessManager> getptr();
	
	/** How much of the reserved memory is available?
	 * 
	 * Decreased by the expected memory-consumption of a process, when
	 * a process starts.
	 * Increased by the expected memory-consumption of a process, when
	 * a process stops.
	 * 
	 * @return Estimated free memory.
	 */
	long long reservedMemAvail();
	
	/** How many processes are free?
	 * 
	 * Decremented, when a process starts.
	 * Incremented, when a process stops.
	 * 
	 * @return Number of free process-slots.
	 */
	int reservedProcessesAvail();
	
	/** Enqueue a Process which has not been started yet.
	 */
	void addProcess(
		std::shared_ptr<Process> pr,		///< shared pointer to process-description, which is about to be started by the ProcessManager
		long long expectedMemConsumptionkB	///< expected memoryconsumption of the process in kB
		
	);
	
	/** Handle incoming process-output, deliver input to processes and
	 * spawn an new process when resources are sufficient.
	 */
	void iterate();
	
	/** Function to tell the ProcessManager, a process has ended and can
	 * be removed from the Process-map.
	 */
	void queueElementFinished(int pid);

private:
	ProcessManager(int maxProcesses, long long reserveMaxMemory, std::shared_ptr<EpollReactor> rea);
	
	std::list<std::shared_ptr<QueueElement>> queue_waiting;
	std::map<int, long long> queue_running; // <pid, mem-expected>, to keep reserved_mem_avail up to date.
	long long reserved_mem, reserved_mem_avail;
	int reserved_processes, reserved_processes_avail;
	
	std::shared_ptr<EpollReactor> reactor;
};

#endif
